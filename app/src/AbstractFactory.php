<?php

declare(strict_types=1);

namespace Factory;

abstract class AbstractFactory
{
    abstract public function makeBook(BookType $type): AbstractBook;
}
