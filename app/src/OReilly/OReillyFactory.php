<?php

declare(strict_types=1);

namespace Factory\OReilly;

use Factory\AbstractFactory;
use Factory\BookType;
use Factory\Sams\SamsBook;

class OReillyFactory extends AbstractFactory
{
    public function makeBook(BookType $type): SamsBook|OReillyBook
    {
        return match ($type) {
            BookType::SAMS => new SamsBook(author: SamsBook::BOOK_AUTHOR, title: SamsBook::BOOK_TITLE),
            default => new OReillyBook(author: OReillyBook::BOOK_AUTHOR, title: OReillyBook::BOOK_TITLE),
        };
    }
}
