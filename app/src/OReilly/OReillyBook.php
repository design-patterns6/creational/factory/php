<?php

declare(strict_types=1);

namespace Factory\OReilly;

use Factory\AbstractBook;

class OReillyBook extends AbstractBook
{
    final public const BOOK_AUTHOR = 'Rasmus Lerdorf and Kevin Tatroe';
    final public const BOOK_TITLE = 'Programming PHP';
}
