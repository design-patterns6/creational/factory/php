<?php

declare(strict_types=1);

namespace Factory;

enum BookType
{
    case OREILLY;
    case SAMS;
}
