<?php

declare(strict_types=1);

namespace Factory\Sams;

use Factory\AbstractFactory;
use Factory\BookType;
use Factory\OReilly\OReillyBook;

class SamsFactory extends AbstractFactory
{
    public function makeBook(BookType $type): SamsBook|OReillyBook
    {
        return match ($type) {
            BookType::OREILLY => new OReillyBook(author: OReillyBook::BOOK_AUTHOR, title: OReillyBook::BOOK_TITLE),
            default => new SamsBook(author: SamsBook::BOOK_AUTHOR, title: SamsBook::BOOK_TITLE),
        };
    }
}
