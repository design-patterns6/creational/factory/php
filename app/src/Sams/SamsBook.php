<?php

declare(strict_types=1);

namespace Factory\Sams;

use Factory\AbstractBook;

class SamsBook extends AbstractBook
{
    final public const BOOK_AUTHOR = 'George Schlossnagle';
    final public const BOOK_TITLE = 'Advanced PHP Programming';
}
