<?php

declare(strict_types=1);

namespace Factory;

abstract class AbstractBook
{
    public function __construct(
        protected readonly string $author,
        protected readonly string $title,
    ) {
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
