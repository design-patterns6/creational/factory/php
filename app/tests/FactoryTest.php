<?php

declare(strict_types=1);

namespace Factory\Tests;

use Factory\BookType;
use Factory\OReilly\OReillyBook;
use Factory\OReilly\OReillyFactory;
use Factory\Sams\SamsBook;
use Factory\Sams\SamsFactory;
use PHPUnit\Framework\TestCase;

class FactoryTest extends TestCase
{
    public function testOReillyFactoryWithOreillyBookTypeShouldReturnOreillyBook(): void
    {
        // GIVEN
        $oreillyFactoryMethod = new OReillyFactory();
        $type = BookType::OREILLY;
        // WHEN
        $oreillyBook = $oreillyFactoryMethod->makeBook(type: $type);
        // THEN
        self::assertInstanceOf(expected: OReillyBook::class, actual: $oreillyBook);
        self::assertEquals(expected: OReillyBook::BOOK_AUTHOR, actual: $oreillyBook->getAuthor());
        self::assertEquals(expected: OReillyBook::BOOK_TITLE, actual: $oreillyBook->getTitle());
    }

    public function testOReillyFactoryWithSamsBookTypeShouldReturnSamsBook(): void
    {
        // GIVEN
        $oreillyFactoryMethod = new OReillyFactory();
        $type = BookType::SAMS;
        // WHEN
        $samsBook = $oreillyFactoryMethod->makeBook(type: $type);
        // THEN
        self::assertInstanceOf(expected: SamsBook::class, actual: $samsBook);
        self::assertEquals(expected: SamsBook::BOOK_AUTHOR, actual: $samsBook->getAuthor());
        self::assertEquals(expected: SamsBook::BOOK_TITLE, actual: $samsBook->getTitle());
    }

    public function testSamsFactoryWithSamsBookTypeShouldReturnSamsBook(): void
    {
        // GIVEN
        $samsFactoryMethod = new SamsFactory();
        $type = BookType::SAMS;
        // WHEN
        $samsBook = $samsFactoryMethod->makeBook(type: $type);
        // THEN
        self::assertInstanceOf(expected: SamsBook::class, actual: $samsBook);
        self::assertEquals(expected: SamsBook::BOOK_AUTHOR, actual: $samsBook->getAuthor());
        self::assertEquals(expected: SamsBook::BOOK_TITLE, actual: $samsBook->getTitle());
    }

    public function testSamsFactoryWithOreillyBookTypeShouldReturnOreillyBook(): void
    {
        // GIVEN
        $samsFactoryMethod = new SamsFactory();
        $type = BookType::OREILLY;
        // WHEN
        $oreillyBook = $samsFactoryMethod->makeBook(type: $type);
        // THEN
        self::assertInstanceOf(expected: OReillyBook::class, actual: $oreillyBook);
        self::assertEquals(expected: OReillyBook::BOOK_AUTHOR, actual: $oreillyBook->getAuthor());
        self::assertEquals(expected: OReillyBook::BOOK_TITLE, actual: $oreillyBook->getTitle());
    }
}
